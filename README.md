# Introducción a PySpark

La información de este repositorio se basa en un video tutorial sobre PySpark que se encuentra en [este enlace de YouTube](https://www.youtube.com/watch?v=_C8kWso4ne4).

Se transcribe el video en [este documento de Google Colab](https://colab.research.google.com/drive/1pf3Ntvm4cvCVf8xdWZYOHVtD6X0N_rHF?usp=sharing).
